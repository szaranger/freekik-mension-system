Case
We are developing a social network and we want to implement a mention system for letting users name each other.

Description
When a user wants to mention another user in a publication, it can be achieved by typing the special ‘@‘ character and start typing the name of the mentioned user. When this happens, we will detect that a mention needs to be inserted and propose a list of matching users from a custom list of users.

Example of a mention: Hello @Freekik!!

Requirements
Devise and structure a system for mentions.

Technical hints
Use GitHub users API: https://developer.github.com/v3/users/

Help
If you get stuck or you need any help don’t hesitate to contact us so we can give you some hints, we want to see how robust and efficient is your code even if we have to give you the bases: gregoire.frileux@freekik.com

Choose one test:

Test 1
Create an Input component that allows you to add mentions in the text.

When an ‘@‘ is written, the component will detect the next character introduced and a list will come up showing the users whose names start with that character. If one of the users from the list is selected, it will appear in the text and the '@' as well as the text added after the '@' will disappear.

If a character of the mention is deleted, the whole mention disappears.

Once the user finishes inserting the text, he will be able to press a button "Post". When this button is pressed, it will create an object (think about the structure of this object) for storing the mentions.

Added value
- Use Redux if you find a way it can be helpful
- Create the component in a more generic way for supporting many kinds of “text enrichers”, for example tags (#Barcelona). The algorithm for this is not necessary, just leave the component ready for it.

Test 2
Create a Text component that is able to represent mentions with a different color in the text and allows you to do an action when you press it.

Added value
- Create the component in a more generic way for supporting other kinds of “text enrichers”, for example tags (#Barcelona). The algorithm for this is not necessary, just leave the component ready for it.

COPY PASTE IS NOT ALLOWED !!!!

Have fun ;)