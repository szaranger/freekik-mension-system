// @flow
import React from 'react';
import styled from 'styled-components';

const StyledMention = styled.strong`
  font-weight: inherit;
`;

type Props = {
  display: string,
  style: Object,
  onAdd: Function,
  onRemove: Function,
  renderSuggestion: Function,
  trigger: string | RegExp,
  isLoading: boolean
};

const Mention = (props: Props) => (
  <StyledMention style={props.style}>{props.display}</StyledMention>
); 

Mention.defaultProps = {
  trigger: '@',
  onAdd: () => {},
  onRemove: () => {},
  renderSuggestion: null,
  isLoading: false,
  appendSpaceOnAdd: true
};

export default Mention;
