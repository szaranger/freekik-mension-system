import React from 'react';

import { Mention, MentionsInput } from './index';

import Enhancer from './Enhancer';
import defaultStyle from './defaultStyle';

const defaultMentionStyle = {
  backgroundColor: '#cee4e5'
}

const Input = ({ value, data, onChange, onAdd }) => {
  return (
    <div className="single-line">
      <h3>Single line input</h3>

      <MentionsInput
        singleLine
        value={value}
        onChange={onChange}
        style={defaultStyle}
        placeholder={"Mention people using '@'"}
      >
        <Mention data={data} onAdd={onAdd} style={defaultMentionStyle} />
      </MentionsInput>
    </div>
  );
}

const withMention = Enhancer('');

export default withMention(Input);
