import {
  FETCH_USERS,
  RECEIVE_USERS
} from '../actions/actionTypes';

const initialState = { users: [] };

export default function users(state = initialState, action) {
  let newState;
  switch (action.type) {
    case FETCH_USERS:
      return action;
    case RECEIVE_USERS:
      return {
        ...state,
        users: action.users
      };
    default:
      return state;
  }
}