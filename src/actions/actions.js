import * as types from './actionTypes';

function url() {
  return 'https://developer.github.com/v3/users/';
}

export function receiveUsers(json) {
  return {
    type: types.RECEIVE_USERS,
    users: json.users
  };
}

export function fetchUsers() {
  return dispatch => {
    return fetch(url(), {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        headers: {
          // 'x-api-key': apiKey,
          'Accept': 'application/json'
        }
      })
      .then(response => response.json())
      .then(json => dispatch(receiveUsers(json)));
  };
}