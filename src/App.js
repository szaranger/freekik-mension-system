import React, { Component } from 'react';
import Input from './components/Input';
import './App.css';

const users = [{
    id: 'jerry',
    display: 'Jerry Seinfeld',
  },
  {
    id: 'george',
    display: 'George Costanza',
  },
  {
    id: 'cosmo',
    display: 'Kosmo Kramer',
  },
  {
    id: 'elaine',
    display: 'Elaine Benes',
  },
  {
    id: 'susan',
    display: 'Susan Ross',
  },
  {
    id: 'newman',
    display: 'Newman',
  },
  {
    id: 'frank',
    display: 'Frank Costanza',
  },
];

class App extends Component {
  render() {
    return <Input data={users} />;
  }
}

export default App;
